FROM registry.gitlab.com/krillr-docker/archlinux-desktop:master
MAINTAINER Aaron Krill <aaron@krillr.com>

### Update mirror list
RUN curl -o /etc/pacman.d/mirrorlist "https://www.archlinux.org/mirrorlist/?country=US&protocol=https&ip_version=6&use_mirror_status=on" && \
  sed -i 's/^#//' /etc/pacman.d/mirrorlist

### Set up the local user
RUN useradd -m -G users,wheel,audio krillr

### Switch to build user
USER krillr
WORKDIR /home/krillr

# Install local user's preferred packages
RUN pacaur --noconfirm --noedit -Syu i3-gaps \
                                     i3blocks \
                                     conky \
                                     discord \
                                     slack \
                                     touchegg-git \
                                     playerctl \
                                     spotify \
                                     keybase-bin \
                                     compton-conf \
                                     awesome-terminal-fonts \
                                     powerline-fonts-git \
                                     powerline \
                                     feh \
                                     rofi \
                                     firefox \
                                     compton \
                                     pcsclite && \
    sudo rm -rf /var/cache/pacman/pkg/*.xz && \
    rm -rf $HOME/.cache

# Now for the dot files...
RUN git clone --bare https://gitlab.com/krillr/dotfiles .files && \
    git --git-dir=.files --work-tree=. checkout && \
    git --git-dir=.files --work-tree=. submodule update --init --recursive

# Add XOrg config files. We'll see if we really need these later
COPY etc/X11/xorg.conf.d/* /etc/X11/xorg.conf.d/
COPY etc/udev/rules.d/* /etc/udev/rules.d/

VOLUME "/"
CMD startx -- :1

